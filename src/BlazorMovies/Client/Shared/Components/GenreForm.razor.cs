﻿using BlazorMovies.Shared.Entities;
using Microsoft.AspNetCore.Components;

namespace BlazorMovies.Client.Shared.Components
{
    public partial class GenreForm
    {
        [Parameter] public Genre Genre { get; set; }
        [Parameter] public EventCallback OnValidSubmit { get; set; }
    }
}
