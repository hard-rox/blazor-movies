﻿using System;
using BlazorMovies.Shared.Entities;
using Microsoft.AspNetCore.Components;

namespace BlazorMovies.Client.Shared.Components
{
    public partial class CrewForm
    {
        [Parameter] public Crew Crew { get; set; }
        [Parameter] public EventCallback OnValidSubmit { get; set; }

        private void ImageSelected(string imageBase64String)
        {
            Console.WriteLine(imageBase64String);
        }
    }
}
