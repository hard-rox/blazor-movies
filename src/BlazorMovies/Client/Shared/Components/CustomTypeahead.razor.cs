﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Timers;
using Microsoft.AspNetCore.Components;

namespace BlazorMovies.Client.Shared.Components
{
    public partial class CustomTypeahead<TItem>
    {
        [Parameter] public string Placeholder { get; set; }
        [Parameter] public string Class { get; set; }
        [Parameter] public int MinLength { get; set; } = 3;
        [Parameter] public Func<string, Task<IEnumerable<TItem>>> SearchMethod { get; set; }
        [Parameter] public EventCallback<TItem> ResultSelected { get; set; }
        [Parameter] public RenderFragment<TItem> ResultTemplate { get; set; }
        [Parameter] public RenderFragment NotFoundTemplate { get; set; }

        private bool _isSearching;
        private bool _isShowingResult;
        private bool _isMouseInResult;
        private Timer _debounceTimer;
        private string _searchText = string.Empty;
        private List<TItem> _searchResult;

        private string SearchText
        {
            get => _searchText;
            set
            {
                _searchText = value;
                if (value.Length >= MinLength)
                {
                    _debounceTimer.Stop();
                    _debounceTimer.Start();
                }
                else
                {
                    _debounceTimer.Stop();
                    _isShowingResult = false;
                    _searchResult = new List<TItem>();
                }
            }
        }

        protected override void OnInitialized()
        {
            _debounceTimer = new Timer(300) { AutoReset = false };
            _debounceTimer.Elapsed += Search;
            base.OnInitialized();
        }

        private async void Search(object source, ElapsedEventArgs e)
        {
            _isSearching = true;
            _isShowingResult = false;
            await InvokeAsync(StateHasChanged);

            _searchResult = (await SearchMethod.Invoke(_searchText)).ToList();
            _isSearching = false;
            _isShowingResult = true;
            await InvokeAsync(StateHasChanged);

            foreach (var item in _searchResult)
            {
                Console.WriteLine(item.ToString());
            }
        }

        private void ShowResults()
        {
            if (SearchText.Length >= MinLength && _searchResult.Any()) _isShowingResult = true;
        }

        private void HideResults()
        {
            if (!_isMouseInResult)
            {
                _isShowingResult = false;
            }
        }

        private bool ShouldShowResult()
        {
            return _isShowingResult && _searchResult.Any();
        }

        private bool ShowNotFound()
        {
            return _isShowingResult && !_searchResult.Any();
        }

        private async Task SelectResult(TItem item)
        {
            SearchText = "";
            await ResultSelected.InvokeAsync(item);
        }

        private void OnMouseOverResult()
        {
            _isMouseInResult = true;
        }
        private void OnMouseOutResult()
        {
            _isMouseInResult = false;
        }
    }
}
