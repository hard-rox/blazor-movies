﻿using Markdig;
using Microsoft.AspNetCore.Components;

namespace BlazorMovies.Client.Shared.Components
{
    public partial class RenderMarkdown
    {
        [Parameter] public string MarkdownContent { get; set; }

        private string _htmlString;

        protected override void OnParametersSet()
        {
            _htmlString = !string.IsNullOrEmpty(MarkdownContent) 
                ? Markdown.ToHtml(MarkdownContent) : null;
        }
    }
}
