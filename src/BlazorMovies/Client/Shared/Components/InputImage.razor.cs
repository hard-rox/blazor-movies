﻿using System;
using System.Threading.Tasks;
using Blazor.FileReader;
using BlazorMovies.Shared.Utilities;
using Microsoft.AspNetCore.Components;

namespace BlazorMovies.Client.Shared.Components
{
    public partial class InputImage
    {
        [Inject] public IFileReaderService FileReaderService { get; set; }

        [Parameter] public string Label { get; set; } = "Image";
        [Parameter] public string ImageUrl { get; set; }
        [Parameter] public EventCallback<string> OnImageSelect { get; set; }

        private string _imageBase64; //For uploaded image
        private ElementReference _elementReference;

        private async Task OnImageChange()
        {
            var uploadedFiles = await FileReaderService.CreateReference(_elementReference).EnumerateFilesAsync();
            foreach (var fileReference in uploadedFiles)
            {
                _imageBase64 = await fileReference.FileReferenceToBase64Async();
                await OnImageSelect.InvokeAsync(_imageBase64);
                ImageUrl = null;
                StateHasChanged();
            }
        }
    }
}
