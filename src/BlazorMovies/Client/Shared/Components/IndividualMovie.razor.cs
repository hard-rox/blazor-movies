﻿using BlazorMovies.Shared.Entities;
using Microsoft.AspNetCore.Components;

namespace BlazorMovies.Client.Shared.Components
{
    public partial class IndividualMovie
    {
        [Parameter] public Movie Movie { get; set; }

        private string _movieUrl;

        protected override void OnInitialized()
        {
            _movieUrl = $"/movie/{Movie.Id}/{Movie.Title.Replace(' ', '-').ToLower()}";
        }
    }
}
