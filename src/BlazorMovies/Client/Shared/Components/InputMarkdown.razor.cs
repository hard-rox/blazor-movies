﻿using System;
using System.Linq.Expressions;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Components.Forms;

namespace BlazorMovies.Client.Shared.Components
{
    public partial class InputMarkdown<TValue> : InputTextArea
    {
        [Parameter] public string Label { get; set; }
        [Parameter] public Expression<Func<TValue>> For { get; set; }
    }
}
