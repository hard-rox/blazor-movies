﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Components;

namespace BlazorMovies.Client.Shared.Components
{
    public partial class MultipleSelector
    {
        [Parameter] public List<MultipleSelectorItem> Deselected { get; set; }
        [Parameter] public List<MultipleSelectorItem> Selected { get; set; }

        private void Select(MultipleSelectorItem selectedItem)
        {
            Selected.Add(selectedItem);
            Deselected.Remove(selectedItem);
        }

        private void Deselect(MultipleSelectorItem deselectedItem)
        {
            Deselected.Add(deselectedItem);
            Selected.Remove(deselectedItem);
        }

        private void SelectAll()
        {
            Selected.AddRange(Deselected);
            Deselected.Clear();
        }

        private void DeselectAll()
        {
            Deselected.AddRange(Selected);
            Selected.Clear();
        }
    }

    public struct MultipleSelectorItem
    {
        public MultipleSelectorItem(string key, string value)
        {
            Key = key;
            Value = value;
        }

        public string Key { get; private set; }
        public string Value { get; private set; }
    }
}
