﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using BlazorMovies.Shared.Entities;
using Microsoft.AspNetCore.Components;

namespace BlazorMovies.Client.Shared.Components
{
    public partial class MovieForm
    {
        [Parameter] public Movie Movie { get; set; }
        [Parameter] public EventCallback OnValidSubmit { get; set; }

        private List<Genre> _genres;

        private List<MultipleSelectorItem> _deselected = new List<MultipleSelectorItem>();
        private List<MultipleSelectorItem> _selected = new List<MultipleSelectorItem>();
        protected override void OnInitialized()
        {
            _genres = new List<Genre>()
            {
                new Genre() {Id = 1, Name = "Gen 1"},
                new Genre() {Id = 2, Name = "Gen 2"},
                new Genre() {Id = 3, Name = "Gen 3"},
                new Genre() {Id = 4, Name = "Gen 4"},
            };
            Movie.Genres ??= new List<MovieGenre>();
            var movieGenres = Movie.Genres.Select(m => m.Genre).ToList();
            _selected = movieGenres.Select(x => new MultipleSelectorItem(x.Id.ToString(), x.Name))
                .ToList();
            _deselected = _genres.Where(g => !movieGenres.Contains(g))
                .Select(x => new MultipleSelectorItem(x.Id.ToString(), x.Name)).ToList();
        }

        private void PosterSelected(string imageBase64)
        {
            Console.WriteLine(imageBase64);
        }

        private async Task<IEnumerable<Crew>> SearchActorsAsync(string searchText)
        {
            var list = new List<Crew>
            {
                new Crew{Id = 1, Name = "Tom Cruise", Picture = "https://www.biography.com/.image/t_share/MTE5NDg0MDU0OTM2NTg1NzQz/tom-cruise-9262645-1-402.jpg"},
                new Crew{Id = 1, Name = "Tom Holland", Picture = "https://upload.wikimedia.org/wikipedia/commons/3/3c/Tom_Holland_by_Gage_Skidmore.jpg"},
                new Crew{Id = 1, Name = "Emma Watson", Picture = "https://upload.wikimedia.org/wikipedia/commons/7/7f/Emma_Watson_2013.jpg"},
            };
            return list.Where(l => l.Name.ToLower().Contains(searchText.ToLower()));
        }

        private void ResultSelected(string result)
        {
            Console.WriteLine(result);
        }
    }
}
