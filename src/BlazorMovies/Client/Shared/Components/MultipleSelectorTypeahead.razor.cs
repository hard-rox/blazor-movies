﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Components;

namespace BlazorMovies.Client.Shared.Components
{
    public partial class MultipleSelectorTypeahead<T>
    {
        [Parameter] public string Placeholder { get; set; }
        [Parameter] public string InputClass { get; set; }
        [Parameter] public List<T> SelectedElements { get; set; }
        [Parameter] public Func<string, Task<IEnumerable<T>>> SearchMethod { get; set; }
        [Parameter] public RenderFragment<T> SearchResultTemplate { get; set; }
        [Parameter] public RenderFragment<T> SelectedItemTemplate { get; set; }

        private T _draggedItem;

        protected override void OnInitialized()
        {
            SelectedElements = new List<T>();
        }

        private void SelectResult(T item)
        {
            if (!SelectedElements.Contains(item))
            {
                SelectedElements.Add(item);
            }
        }

        private void DragStart(T item)
        {
            _draggedItem = item;
        }

        private void DragOver(T item)
        {
            if (item.Equals(_draggedItem)) return;

            var draggedIdx = SelectedElements.IndexOf(_draggedItem);
            var itemIdx = SelectedElements.IndexOf(item);
            SelectedElements[draggedIdx] = item;
            SelectedElements[itemIdx] = _draggedItem;
        }
    }
}
