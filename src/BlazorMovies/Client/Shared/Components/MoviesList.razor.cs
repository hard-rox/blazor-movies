﻿using System.Collections.Generic;
using BlazorMovies.Shared.Entities;
using Microsoft.AspNetCore.Components;

namespace BlazorMovies.Client.Shared.Components
{
    public partial class MoviesList
    {
        [Parameter] public ICollection<Movie> Movies { get; set; }
    }
}
