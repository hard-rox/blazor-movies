﻿using System;
using BlazorMovies.Shared.Entities;
using Microsoft.AspNetCore.Components;

namespace BlazorMovies.Client.Pages.Genres
{
    public partial class GenreUpdate
    {
        [Parameter] public int GenreId { get; set; }

        private Genre _genre;

        protected override void OnInitialized()
        {
            _genre = new Genre()
            {
                Id = GenreId,
                Name = "Update Genre"
            };
        }

        private void UpdateGenre()
        {
            Console.WriteLine(_genre.Name);
        }
    }
}
