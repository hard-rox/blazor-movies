﻿using System;
using BlazorMovies.Shared.Entities;
using Microsoft.AspNetCore.Components;

namespace BlazorMovies.Client.Pages.Crews
{
    public partial class CrewUpdate
    {
        [Parameter] public int CrewId { get; set; }
        private Crew _crew;

        protected override void OnInitialized()
        {
            _crew = new Crew()
            {
                Id = CrewId,
                Name = "Update Crew"
            };
        }

        private void UpdateCrew()
        {
            Console.WriteLine(_crew.Name);
        }
    }
}
