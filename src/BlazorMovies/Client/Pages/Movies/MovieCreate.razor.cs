﻿using System;
using BlazorMovies.Shared.Entities;
using Microsoft.AspNetCore.Components;

namespace BlazorMovies.Client.Pages.Movies
{
    public partial class MovieCreate
    {
        [Inject] public NavigationManager NavigationManager { get; set; }

        private Movie _movie = new Movie();

        private void SaveMovie()
        {
            Console.WriteLine(_movie.Title);
            //Console.WriteLine(NavigationManager.Uri);
            //NavigationManager.NavigateTo("/movie");
        }
    }
}
