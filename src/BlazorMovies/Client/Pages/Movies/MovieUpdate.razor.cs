﻿using System;
using System.Collections.Generic;
using BlazorMovies.Shared.Entities;
using Microsoft.AspNetCore.Components;

namespace BlazorMovies.Client.Pages.Movies
{
    public partial class MovieUpdate
    {
        [Parameter] public int MovieId { get; set; }

        private Movie _movie;

        protected override void OnInitialized()
        {
            _movie = new Movie()
            {
                Id = MovieId,
                Title = "Testing...",
                Genres = new List<MovieGenre>()
                {
                    new MovieGenre(){MovieId = MovieId, Genre = new Genre(){Id = 1, Name = "Gen 1"}},
                    new MovieGenre(){MovieId = MovieId, Genre = new Genre(){Id = 3, Name = "Gen 3"}},
                }
            };
        }

        private void UpdateMovie()
        {
            Console.WriteLine(_movie.Title);
        }
    }
}
