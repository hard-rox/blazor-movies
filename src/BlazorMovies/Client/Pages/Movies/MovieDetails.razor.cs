﻿using System;
using Microsoft.AspNetCore.Components;

namespace BlazorMovies.Client.Pages.Movies
{
    public partial class MovieDetails
    {
        [Parameter] public int MovieId { get; set; }
        [Parameter] public string MovieName { get; set; }

        protected override void OnInitialized()
        {
            Console.WriteLine(MovieId);
            Console.WriteLine(MovieName);
        }
    }
}
