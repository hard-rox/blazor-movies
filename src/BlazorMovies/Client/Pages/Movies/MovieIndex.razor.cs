﻿using System;
using System.Collections.Generic;
using BlazorMovies.Shared.Entities;

namespace BlazorMovies.Client.Pages.Movies
{
    public partial class MovieIndex
    {
        private ICollection<Movie> _movies;

        protected override void OnInitialized()
        {
            _movies = new List<Movie>(){
                new Movie(){Id = 1, Title = "Movie 1", ReleaseDate = DateTime.Now, Poster = "https://m.media-amazon.com/images/M/MV5BMDJiNzUwYzEtNmQ2Yy00NWE4LWEwNzctM2M0MjE0OGUxZTA3XkEyXkFqcGdeQXVyMTMxODk2OTU@._V1_SY1000_CR0,0,675,1000_AL_.jpg"},
                new Movie(){Id = 1, Title = "Movie 2", ReleaseDate = DateTime.Now, Poster = "https://m.media-amazon.com/images/M/MV5BMDJiNzUwYzEtNmQ2Yy00NWE4LWEwNzctM2M0MjE0OGUxZTA3XkEyXkFqcGdeQXVyMTMxODk2OTU@._V1_SY1000_CR0,0,675,1000_AL_.jpg"},
                new Movie(){Id = 1, Title = "Movie 3", ReleaseDate = DateTime.Now, Poster = "https://m.media-amazon.com/images/M/MV5BMDJiNzUwYzEtNmQ2Yy00NWE4LWEwNzctM2M0MjE0OGUxZTA3XkEyXkFqcGdeQXVyMTMxODk2OTU@._V1_SY1000_CR0,0,675,1000_AL_.jpg"},
                new Movie(){Id = 1, Title = "Movie 4", ReleaseDate = DateTime.Now, Poster = "https://m.media-amazon.com/images/M/MV5BMDJiNzUwYzEtNmQ2Yy00NWE4LWEwNzctM2M0MjE0OGUxZTA3XkEyXkFqcGdeQXVyMTMxODk2OTU@._V1_SY1000_CR0,0,675,1000_AL_.jpg"},
                new Movie(){Id = 1, Title = "Movie 5", ReleaseDate = DateTime.Now, Poster = "https://m.media-amazon.com/images/M/MV5BMDJiNzUwYzEtNmQ2Yy00NWE4LWEwNzctM2M0MjE0OGUxZTA3XkEyXkFqcGdeQXVyMTMxODk2OTU@._V1_SY1000_CR0,0,675,1000_AL_.jpg"},
                new Movie(){Id = 1, Title = "Movie 6", ReleaseDate = DateTime.Now, Poster = "https://m.media-amazon.com/images/M/MV5BMDJiNzUwYzEtNmQ2Yy00NWE4LWEwNzctM2M0MjE0OGUxZTA3XkEyXkFqcGdeQXVyMTMxODk2OTU@._V1_SY1000_CR0,0,675,1000_AL_.jpg"},
            };
        }
    }
}