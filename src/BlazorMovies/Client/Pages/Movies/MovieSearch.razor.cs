﻿using System;
using System.Collections.Generic;
using System.Linq;
using BlazorMovies.Shared.Entities;
using Microsoft.AspNetCore.Components.Web;

namespace BlazorMovies.Client.Pages.Movies
{
    public partial class MovieSearch
    {
        private string _title = "";
        private string _selectedGenre = "0";
        private bool _upcomingReleases;
        private bool _inTheaters;
        private ICollection<Genre> _genres;
        private ICollection<Movie> _movies;

        protected override void OnInitialized()
        {
            _genres = new List<Genre>()
            {
                new Genre() {Id = 1, Name = "Gen 1"},
                new Genre() {Id = 2, Name = "Gen 2"},
            };

            _movies = new List<Movie>(){
                new Movie(){Id = 1, Title = "Movie 1", ReleaseDate = DateTime.Now, Poster = "https://m.media-amazon.com/images/M/MV5BMDJiNzUwYzEtNmQ2Yy00NWE4LWEwNzctM2M0MjE0OGUxZTA3XkEyXkFqcGdeQXVyMTMxODk2OTU@._V1_SY1000_CR0,0,675,1000_AL_.jpg"},
                new Movie(){Id = 1, Title = "Movie 2", ReleaseDate = DateTime.Now, Poster = "https://m.media-amazon.com/images/M/MV5BMDJiNzUwYzEtNmQ2Yy00NWE4LWEwNzctM2M0MjE0OGUxZTA3XkEyXkFqcGdeQXVyMTMxODk2OTU@._V1_SY1000_CR0,0,675,1000_AL_.jpg"},
                new Movie(){Id = 1, Title = "Movie 3", ReleaseDate = DateTime.Now, Poster = "https://m.media-amazon.com/images/M/MV5BMDJiNzUwYzEtNmQ2Yy00NWE4LWEwNzctM2M0MjE0OGUxZTA3XkEyXkFqcGdeQXVyMTMxODk2OTU@._V1_SY1000_CR0,0,675,1000_AL_.jpg"},
                new Movie(){Id = 1, Title = "Movie 4", ReleaseDate = DateTime.Now, Poster = "https://m.media-amazon.com/images/M/MV5BMDJiNzUwYzEtNmQ2Yy00NWE4LWEwNzctM2M0MjE0OGUxZTA3XkEyXkFqcGdeQXVyMTMxODk2OTU@._V1_SY1000_CR0,0,675,1000_AL_.jpg"},
                new Movie(){Id = 1, Title = "Movie 5", ReleaseDate = DateTime.Now, Poster = "https://m.media-amazon.com/images/M/MV5BMDJiNzUwYzEtNmQ2Yy00NWE4LWEwNzctM2M0MjE0OGUxZTA3XkEyXkFqcGdeQXVyMTMxODk2OTU@._V1_SY1000_CR0,0,675,1000_AL_.jpg"},
                new Movie(){Id = 1, Title = "Movie 6", ReleaseDate = DateTime.Now, Poster = "https://m.media-amazon.com/images/M/MV5BMDJiNzUwYzEtNmQ2Yy00NWE4LWEwNzctM2M0MjE0OGUxZTA3XkEyXkFqcGdeQXVyMTMxODk2OTU@._V1_SY1000_CR0,0,675,1000_AL_.jpg"},
            };
        }

        private void TitleKeyPress(KeyboardEventArgs e)
        {
            if (e.Key == "Enter")
            {
                Search();
            }
        }

        private void Search()
        {
            _movies = _movies.Where(m => m.Title.Contains(_title)).ToList();
        }

        private void Clear()
        {
            _title = "";
            _selectedGenre = "0";
            _upcomingReleases = false;
            _inTheaters = false;
            _movies = new List<Movie>(){
                new Movie(){Id = 1, Title = "Movie 1", ReleaseDate = DateTime.Now, Poster = "https://m.media-amazon.com/images/M/MV5BMDJiNzUwYzEtNmQ2Yy00NWE4LWEwNzctM2M0MjE0OGUxZTA3XkEyXkFqcGdeQXVyMTMxODk2OTU@._V1_SY1000_CR0,0,675,1000_AL_.jpg"},
                new Movie(){Id = 1, Title = "Movie 2", ReleaseDate = DateTime.Now, Poster = "https://m.media-amazon.com/images/M/MV5BMDJiNzUwYzEtNmQ2Yy00NWE4LWEwNzctM2M0MjE0OGUxZTA3XkEyXkFqcGdeQXVyMTMxODk2OTU@._V1_SY1000_CR0,0,675,1000_AL_.jpg"},
                new Movie(){Id = 1, Title = "Movie 3", ReleaseDate = DateTime.Now, Poster = "https://m.media-amazon.com/images/M/MV5BMDJiNzUwYzEtNmQ2Yy00NWE4LWEwNzctM2M0MjE0OGUxZTA3XkEyXkFqcGdeQXVyMTMxODk2OTU@._V1_SY1000_CR0,0,675,1000_AL_.jpg"},
                new Movie(){Id = 1, Title = "Movie 4", ReleaseDate = DateTime.Now, Poster = "https://m.media-amazon.com/images/M/MV5BMDJiNzUwYzEtNmQ2Yy00NWE4LWEwNzctM2M0MjE0OGUxZTA3XkEyXkFqcGdeQXVyMTMxODk2OTU@._V1_SY1000_CR0,0,675,1000_AL_.jpg"},
                new Movie(){Id = 1, Title = "Movie 5", ReleaseDate = DateTime.Now, Poster = "https://m.media-amazon.com/images/M/MV5BMDJiNzUwYzEtNmQ2Yy00NWE4LWEwNzctM2M0MjE0OGUxZTA3XkEyXkFqcGdeQXVyMTMxODk2OTU@._V1_SY1000_CR0,0,675,1000_AL_.jpg"},
                new Movie(){Id = 1, Title = "Movie 6", ReleaseDate = DateTime.Now, Poster = "https://m.media-amazon.com/images/M/MV5BMDJiNzUwYzEtNmQ2Yy00NWE4LWEwNzctM2M0MjE0OGUxZTA3XkEyXkFqcGdeQXVyMTMxODk2OTU@._V1_SY1000_CR0,0,675,1000_AL_.jpg"},
            };
        }
    }
}
