﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace BlazorMovies.Shared.Entities
{
    public class Movie
    {
        public int Id { get; set; }
        [Required]
        public string Title { get; set; }
        [Required]
        public string Plot { get; set; }
        [Required]
        public string Trailer { get; set; }
        public bool InTheaters { get; set; }
        [Required]
        public DateTime? ReleaseDate { get; set; }
        public string Poster { get; set; }

        public virtual ICollection<MovieGenre> Genres { get; set; }
    }
}
