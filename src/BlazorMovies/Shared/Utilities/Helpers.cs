﻿using System;
using System.Threading.Tasks;
using Blazor.FileReader;

namespace BlazorMovies.Shared.Utilities
{
    public static class Helpers
    {
        public static async Task<string> FileReferenceToBase64Async(this IFileReference reference)
        {
            await using var stream = await reference.CreateMemoryStreamAsync(4*1024);
            var imageBytes = new byte[stream.Length];
            stream.Read(imageBytes, 0, (int) stream.Length);
            var base64String = Convert.ToBase64String(imageBytes);
            return base64String;
        }
    }
}
