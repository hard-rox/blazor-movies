# BehudaBlogGraphQL

![HotChocolate](https://chillicream.com/img/projects/hotchocolate-banner.svg)

**BehudaBlogGraphQL** is an ASP.NET Core GraphQL API, guinea pig of my GraphQL learning mission. This is developed using very popular GraphQL package [HotChocolate](https://github.com/ChilliCream/hotchocolate). This is a simple blog site. Any suggestions, issues will be wolcomed.

## Getting Started

To getting started with this project:
1. Clone the repository
2. Run **Update&Run.bat** file.
    This file will update the repo into latest version, update local database, build the project and run it using DotNet CLI.
    If you want you can enter the commands as your own.
    ```bash
    git pull origin master
    dotnet ef database update --project project src/BehudaBlog.GraphQL.Api
    dotnet build
    dotnet run --project project src/BehudaBlog.GraphQL.Api
    ```

### Prerequisites

Things have to installed to continue...
1. [DotNet Core SDK 3.1.X or higher](https://dotnet.microsoft.com/download).
2. [MS SQL Server](https://www.microsoft.com/en-us/sql-server/sql-server-downloads).
Pretty much it.


## Built With

* [DotNet Core SDK 3.1.X](https://dotnet.microsoft.com/download) - The framework used
* [MS SQL Server](https://www.microsoft.com/en-us/sql-server/sql-server-downloads). - Database Engine
* [Visual Studio 2019](https://visualstudio.microsoft.com/downloads/) - IDE
* [SQL Server Management Studio](https://docs.microsoft.com/en-us/sql/ssms/download-sql-server-management-studio-ssms?view=sql-server-2017) - Database Management Studio
* [Git](https://git-scm.com/downloads) - For versioning.

## Authors

* **Rasedur Rahman Roxy** - *Planning and Development* - [Twitter](https://twitter.com/roxyxmw?lang=en)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments
* [HotChocolate](https://github.com/ChilliCream/hotchocolate) for their awesome project.
* [Pascal Senn](https://twitter.com/Pascal_Senn) for his valuable time to educate me.
* Hat tip to anyone whose code was used
